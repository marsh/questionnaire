% questionnaire.cls
%
% A LaTeX2e document class for preparing questionnaires for human-subjects experiments.

%% questionnaire.cls
%% Copyright (c) 2015 Eric Marsh
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3c
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3c or later is part of all distributions of LaTeX
% version 2008/05/04 or later.
%
% This work has the LPPL maintenance status "maintained".
% 
% This work consists of the files questionnaire.cls and VARI3-demographic.tex.


%%% Eric Marsh
%%% Institut Image
%%% Arts et Métiers ParisTech
%%% 71100 Chalon-sur-Saône, France
%%% bits@wemarsh.com
%%% https://wemarsh.com/

% The newest version of this documentclass should always be available
% from BitBucket: http://bits.wemarsh.com/


\def\fileversion{0.2}
\def\filedate{2015/02/26}


\LoadClass{article}

%typography
\RequirePackage{
	setspace,							%control line spacing						
	pifont							%to use dingbats
}

%element formatting
\RequirePackage{
	enumitem,							%to resume numbering in enumerate environments
	titlesec,							%to modify the section title format
	fancyhdr							%for headers and footers
}

%page formatting
\RequirePackage[margin=0.75in]{geometry}	%set the margins

%needed functionality
\RequirePackage{lastpage}				%needed for page number denominator
\RequirePackage{forloop}
\RequirePackage{array}

\def\@maketitle{%
	\begin{center}%
	\textbf{\Large \@title}%
	\end{center}%
}
\newcommand{\answerblankcm}[1]{%		%for questions of the type, "fill in the blank"
	\enspace\makebox[#1cm]{\hrulefill}%
}
\newcommand{\checkbox}[1]{%				%for responses of the type, "check all that apply"
	\item[\ding{113}]#1%
}
\newcommand{\circlechoice}[1]{%			%for responses of the type, "circle one"
	\hspace{8mm}#1%
}
\newcommand{\subjectID}[0]{%				%subject ID
	ID:\answerblankcm{1}%
}
\newcommand{\subjectgroup}[0]{%			%subject group
	Condition:\answerblankcm{2}%
}
\newcommand{\sessiondate}[0]{%			%session date
	Date:\answerblankcm{3}%
}
\newcommand{\makeheader}[1]{%			%header with study name and page number
	\fancyhead[L]{#1}%
	\fancyhead[R]{Page \thepage /\pageref{LastPage}}%
}
\newcommand{\makefooter}[0]{%			%footer with subject ID, subject group, and session date
	\fancyfoot[L]{\subjectID \hspace{0.5cm} \subjectgroup}%
	\fancyfoot[R]{\sessiondate}%
}
\newcommand{\instructions}[1]{	%			%instructions text
	\underline{Instructions:}\enspace#1%
}
\newcommand{\followupQs}[1]{%			%for questions related to the previous question (e.g., "If yes:" or "If no:")
	\par\nobreak#1%
}
\newenvironment{vartab}[1]
{
    \begin{tabular}{ c@{} *{#1}{>{\centering\arraybackslash}p{1.4cm}}}
}{
    \end{tabular}
}
\newcommand{\likert}[4]{%				%Likert scale response type
	\setcounter{likertcounter}{0}
	#2\newline
	\begin{vartab}{#1}%
	&#3%
	\forloop{likertcounter}{1}{\value{likertcounter} < #1}{%
		&%
	}#4\\
	\forloop{likertcounter}{0}{\value{likertcounter} < #1}{%
		&\ding{113}%
	}\\
	\setcounter{likertcounter}{0}
	\forloop{likertcounter}{1}{\not \value{likertcounter} > #1}{%
		&\arabic{likertcounter}%
	}
	\end{vartab}%
}
\titleformat{\section}%					%section header format
 	{\normalfont\large\bf}{}{0em}{}
\newcommand{\configurepage}[1]{%			%configure the page
	\setlength\parindent{0pt}%
	\let\ps@plain\ps@fancy
	\fancyhf{}%
	\makeheader{#1}%
	\makefooter
	\doublespacing
	\newcounter{likertcounter}
}

